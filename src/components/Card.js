import React, { useState, useEffect } from 'react'
import servicesService from '../services/services'

import Alert from '@mui/material/Alert'
import AlertTitle from '@mui/material/AlertTitle'
import INTERVAL_TIME from '../utils/config'

const Card = ({ apiName, intervalTime }) => {
  const [info, setInfo] = useState([])

  useEffect(() => {
    function getData () {
      servicesService.getAll(apiName).then(data => {
        setInfo(data)
      })
    }
    getData()
    const interval = setInterval(() => getData(), INTERVAL_TIME)
    return () => {
      clearInterval(interval)
    }
  }, [apiName])

  let time = new Date(info.time).toLocaleString(navigator.language, {
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
  })

  return (
    <>
      <Alert
        severity={info.success ? 'success' : 'error'}
        style={{ height: '200px' }}
      >
        <h3>{apiName.toUpperCase()}</h3>
        {info.message ? (
          <AlertTitle>Healthy</AlertTitle>
        ) : (
          <AlertTitle>Error</AlertTitle>
        )}
        {info.hostname ? <p>{info.hostname}</p> : <p>OUTAGE 403 Forbidden</p>}
        {time !== 'Invalid Date' ? time : null}
      </Alert>
    </>
  )
}

export default Card
