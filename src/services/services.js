/* eslint-disable import/no-anonymous-default-export */
import axios from 'axios'

const getAll = API_NAME => {
  const request = axios.get(
    `https://api.factoryfour.com/${API_NAME}/health/status`
  )
  return request.then(response => response.data)
}

export default {
  getAll
}
