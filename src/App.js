import { useMemo } from 'react'
import Card from './components/Card'
import CssBaseline from '@mui/material/CssBaseline'
import useMediaQuery from '@mui/material/useMediaQuery'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Container from '@mui/material/Container'
import BasicAppBar from './components/AppBar'
import { ThemeProvider, createTheme } from '@mui/material/styles'

const apiNames = [
  'accounts',
  'assets',
  'customers',
  'datapoints',
  'devices',
  'documents',
  'forms',
  'invites',
  'media',
  'messages',
  'namespaces',
  'orders',
  'patients',
  'relationships',
  'rules',
  'templates',
  'users',
  'workflows'
]

function App () {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')

  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? 'dark' : 'light'
        }
      }),
    [prefersDarkMode]
  )

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BasicAppBar title='Status Dashboard' />

      <Container maxWidth='xl'>
        <Box sx={{ flexGrow: 1, paddingTop: 4 }}>
          <Grid
            container
            spacing={{ xs: 2, md: 1 }}
            columns={{ xs: 2, sm: 4, lg: 12 }}
          >
            {apiNames.map(apiName => (
              <Grid item xs={3} md={2}>
                <Card key={apiName} apiName={apiName} />
              </Grid>
            ))}
          </Grid>
        </Box>
      </Container>
    </ThemeProvider>
  )
}

export default App
