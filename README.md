<div id="top"></div>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/sebastiantorres86/status-page">
    <img src="images/FactoryFour Logo.png" alt="Logo" >
  </a>

<h3 align="center">FactoryFour Interview | Status Page</h3>

  <p align="center">
    A status page for the FactoryFour APIs.
    <br />
    <a href="https://status-page-mocha.vercel.app/">View Demo</a>

  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://status-page-mocha.vercel.app/)

OBJECTIVE: Write a status page for the FactoryFour APIs.

STACK: This should be a single-page web application written using React in either TypeScript or JavaScript. HTTP calls can be made using any library.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

- [React.js](https://reactjs.org/)
- [Material UI]()
- [Axios]()

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

- npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/sebastiantorres86/status-page
   ```
1. Install NPM packages
   ```sh
   npm install
   ```
1. Modify the interval between api calls in `utils/config.js`
   ```js
   const INTERVAL_TIME = <milliseconds>
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Sebastian Torres - [@seba_torres_dev](https://twitter.com/seba_torres_dev) - seba.torres.dev@gmail.com

Project Link: [https://gitlab.com/sebastiantorres86/status-page](https://gitlab.com/sebastiantorres86/status-page)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->

[product-screenshot]: images/status-page.png
